##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE cust_rec RECORD
             cust_num INTEGER,
             cust_name VARCHAR(30),
             order_ids DYNAMIC ARRAY OF INTEGER
         END RECORD
  DEFINE obj util.JSONObject
    LET cust_rec.cust_num = 345
    LET cust_rec.cust_name = "McMaclum"
    LET cust_rec.order_ids[1] = 4732
    LET cust_rec.order_ids[2] = 9834
    LET cust_rec.order_ids[3] = 2194
    
    LET obj = util.JSONObject.fromFGL(cust_rec)
    DISPLAY obj.toString()
    
    
  CALL fgl_getkey()
END MAIN