##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
    DEFINE cust_rec RECORD
               cust_num INTEGER,
               cust_name VARCHAR(30),
               order_ids DYNAMIC ARRAY OF INTEGER
           END RECORD
    DEFINE js STRING
    DEFINE json_obj util.JSONObject
    
    LET js='{ "cust_num":35, "cust_name":"McCarlson",
              "order_ids":[234,3456,24656,34561] }'
    LET json_obj = util.JSONObject.parse(js)
    CALL json_obj.toFGL(cust_rec)
    DISPLAY util.JSON.format(js)
    
  CALL fgl_getkey()  
END MAIN


{MAIN
    DEFINE cust_rec RECORD
               cust_num INTEGER,
               cust_name VARCHAR(30),
               order_ids DYNAMIC ARRAY OF INTEGER
           END RECORD
    DEFINE js STRING
    DEFINE json_obj util.JSONObject}
    
--    LET js='{ "cust_num":35, "cust_name":"McCarlson", "order_ids":[234,3456,24656,34561] }'
{    LET json_obj = util.JSONObject.parse(js)
    CALL json_obj.toFGL(cust_rec)
    
    DISPLAY "cust_name = ", cust_rec.cust_name
    DISPLAY "order_ids = ", cust_rec.order_ids
    
  CALL fgl_getkey()  
END MAIN}