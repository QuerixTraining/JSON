##########################################################################
# JSON Project          					                             #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE json_arr util.JSONArray
    LET json_arr = util.JSONArray.parse('[123,"abc",null]')
    DISPLAY json_arr.getLength()
  CALL fgl_getkey()	
END MAIN


{MAIN
  DEFINE da DYNAMIC ARRAY OF INTEGER
    DEFINE json_arr util.JSONArray
      LET da[1] = 1
      LET da[2] = 12
      LET da[3] = 123
      LET da[4] = 1234
      LET da[5] = 12345
      LET json_arr = util.JSONArray.fromFGL(da)
    DISPLAY json_arr.getLength()
  CALL fgl_getkey()	
END MAIN}
