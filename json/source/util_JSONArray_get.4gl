##########################################################################
# JSON Project                    						                 #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
    DEFINE json_arr util.JSONArray
    LET json_arr = util.JSONArray.parse('[123,"abc"]')
		DISPLAY json_arr.get(1)
		DISPLAY json_arr.get(2)
  CALL fgl_getkey()				
END MAIN


