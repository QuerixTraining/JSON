##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE json_obj, obj util.JSONObject
    IF json_obj IS NOT NULL THEN DISPLAY "Failed to DEFINE the object" END IF
      LET json_obj = util.JSONObject.CREATE()
    IF json_obj IS NULL THEN DISPLAY "Failed to CREATE the object" ELSE 
      DISPLAY "PASSED"
    END IF
  CALL fgl_getkey()
END MAIN