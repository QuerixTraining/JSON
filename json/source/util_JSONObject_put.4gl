##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

{MAIN
  DEFINE json_obj util.JSONObject
    
    LET json_obj = util.JSONObject.create()
    CALL json_obj.put("simple_string", "Test string")
    DISPLAY json_obj.toString()  
    
  CALL fgl_getkey()
END MAIN}

MAIN
 DEFINE json_obj util.JSONObject
   
   LET json_obj = util.JSONObject.create()
   CALL json_obj.put("simple_numeric", 12345)
   DISPLAY json_obj.toString()  
   
 CALL fgl_getkey()
END MAIN

{MAIN
    DEFINE json_obj util.JSONObject
    
    DEFINE cust_rec RECORD
               cust_id INTEGER,
               cust_name STRING
           END RECORD
    
    LET json_obj = util.JSONObject.create()
    
    LET cust_rec.cust_id = 15
    LET cust_rec.cust_name = "Barton, Timothy"
    
    CALL json_obj.put("record", cust_rec)
    
    DISPLAY json_obj.toString()  
    
    CALL fgl_getkey()
END MAIN}

{MAIN
    DEFINE json_obj util.JSONObject
    
    DEFINE cust_arr DYNAMIC ARRAY OF INTEGER
    
    LET json_obj = util.JSONObject.create()
    
    LET cust_arr[1] = 1
    LET cust_arr[2] = 12
    LET cust_arr[3] = 123

    CALL json_obj.put("array", cust_arr)
    
    DISPLAY json_obj.toString()  
    
    CALL fgl_getkey()
END MAIN}

{MAIN
  DEFINE json_obj util.JSONObject
    
    DEFINE cust_rec RECORD
               cust_id INTEGER,
               cust_name STRING
           END RECORD
  
  DEFINE cust_arr DYNAMIC ARRAY OF INTEGER
    
    LET json_obj = util.JSONObject.create()
    
    LET cust_rec.cust_id = 15
    LET cust_rec.cust_name = "Barton, Timothy"
    
    LET cust_arr[1] = 1
    LET cust_arr[2] = 12
    LET cust_arr[3] = 123
    
    CALL json_obj.put("simple_string", "Test string")
    CALL json_obj.put("simple_numeric", "12345")
    CALL json_obj.put("record", cust_rec)
    CALL json_obj.put("array", cust_arr)
    
    DISPLAY json_obj.toString()
  CALL fgl_getkey()    
END MAIN}

{MAIN
  DEFINE json_obj util.JSONObject
    
    LET json_obj = util.JSONObject.create()
    CALL json_obj.put("simple_string", "Test string")
      DISPLAY json_obj.toString()  
    CALL json_obj.put("simple_string", "New test string")
      DISPLAY json_obj.toString()  
    
  CALL fgl_getkey()
END MAIN}