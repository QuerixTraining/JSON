##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE json_obj util.JSONObject
    LET json_obj = util.JSONObject.create()
    CALL json_obj.put("address", "5 Brando Street")
    CALL json_obj.remove("address")
    DISPLAY json_obj.get("address")
  CALL fgl_getkey()
END MAIN



{MAIN
    DEFINE json_obj util.JSONObject
    DEFINE js STRING
    
    LET json_obj = util.JSONObject.create()
    
    CALL json_obj.put("id", 8723)
    CALL json_obj.put("name", "McMaclum")
    CALL json_obj.put("address", "5 Brando Street")
    CALL json_obj.put("position", "staff")
    LET js =  json_obj.toString()
    DISPLAY util.JSON.format(js)
    
    CALL json_obj.remove("address")
    LET js =  json_obj.toString()
    DISPLAY util.JSON.format(js)
    
  CALL fgl_getkey()
END MAIN}



{MAIN
    DEFINE json_obj util.JSONObject
    DEFINE cust_rec RECORD
             cust_num INTEGER,
             cust_name VARCHAR(30),
             cust_address VARCHAR(30),
             order_ids DYNAMIC ARRAY OF INTEGER
         END RECORD
    DEFINE js STRING
    
    LET cust_rec.cust_num = 345
    LET cust_rec.cust_name = "McMaclum"
    LET cust_rec.cust_address = "5 Brando Street"
    LET cust_rec.order_ids[1] = 4732
    LET cust_rec.order_ids[2] = 9834
    
    LET json_obj = util.JSONObject.fromFGL(cust_rec)
    LET js =  json_obj.toString()
    DISPLAY util.JSON.format(js)
    
    CALL json_obj.remove("cust_address")
    LET js =  json_obj.toString()
    DISPLAY util.JSON.format(js)
    
  CALL fgl_getkey()
END MAIN}