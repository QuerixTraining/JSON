##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE json_arr util.JSONArray
    DEFINE da DYNAMIC ARRAY OF STRING
    LET json_arr = util.JSONArray.parse('["aa","bb","cc"]')
    CALL json_arr.toFGL(da)
    DISPLAY da
  CALL fgl_getkey()
END MAIN