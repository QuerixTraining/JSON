##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE json_arr util.JSONArray
    LET json_arr = util.JSONArray.create()
    CALL json_arr.put(1, 123)
    CALL json_arr.put(2, "abc")
    DISPLAY json_arr.toString()  
  CALL fgl_getkey()
END MAIN


{MAIN
    DEFINE json_arr util.JSONArray
    DEFINE cust_rec RECORD
               cust_id INTEGER,
               cust_name STRING
           END RECORD
    
    LET json_arr = util.JSONArray.create()
    
    LET cust_rec.cust_id = 15
    LET cust_rec.cust_name = "Barton, Timothy"
    
    CALL json_arr.put(1, cust_rec)
    
    DISPLAY json_arr.toString()  
    
    CALL fgl_getkey()
END MAIN}



{MAIN
  DEFINE json_arr util.JSONArray
  DEFINE da DYNAMIC ARRAY OF INTEGER
    LET json_arr = util.JSONArray.create()
    LET da[1] = 1
    LET da[2] = 12
    LET da[3] = 123
    CALL json_arr.put(1, da)
    DISPLAY json_arr.toString()  
  CALL fgl_getkey()
END MAIN}

{MAIN
  DEFINE json_arr util.JSONArray
    LET json_arr = util.JSONArray.create()
    CALL json_arr.put(1, 123)
    CALL json_arr.put(2, "test string")
    DISPLAY json_arr.toString()
    
    CALL json_arr.put(2, "new test string")
    DISPLAY json_arr.toString()
  CALL fgl_getkey()
END MAIN}


