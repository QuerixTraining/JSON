##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE cust_rec RECORD
             cust_num INTEGER,
             cust_name, address VARCHAR(30)
         END RECORD
  DEFINE obj util.JSONObject
    LET cust_rec.cust_num = 75
    LET cust_rec.cust_name = "Ferguson"
    LET cust_rec.address = "12 Marylon Street"
    
    LET obj = util.JSONObject.fromFGL(cust_rec)
    DISPLAY obj.toString() 
    
  CALL fgl_getkey()
END MAIN