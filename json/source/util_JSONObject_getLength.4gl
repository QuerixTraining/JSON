##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE json_obj util.JSONObject
    LET json_obj = util.JSONObject.parse('{"id":123,"name":"Scott"}')
    DISPLAY json_obj.getLength()
  CALL fgl_getkey()	
END MAIN

{MAIN
  DEFINE json_obj util.JSONObject
  DEFINE cust_rec RECORD
           cust_num INTEGER,
           cust_name VARCHAR(30),
           order_id DYNAMIC ARRAY OF INTEGER
         END RECORD

    LET cust_rec.cust_num = 345
    LET cust_rec.cust_name = "McMaclum"
    LET cust_rec.order_id = 4732

    LET json_obj = util.JSONObject.fromFGL(cust_rec)
    DISPLAY json_obj.getLength()
  CALL fgl_getkey()
END MAIN}



