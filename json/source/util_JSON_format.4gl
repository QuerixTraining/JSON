##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

   DEFINE js STRING

   LET js = '{"cust_num":345,"cust_name":"McMaclum","order_ids":[4732,9834,2194]}'
   DISPLAY util.JSON.format( js )
END MAIN

{MAIN
    DEFINE cust_rec RECORD
               cust_num INTEGER,
               cust_name VARCHAR(30),
               order_ids DYNAMIC ARRAY OF INTEGER
           END RECORD
    DEFINE js, s1, s2 STRING
    LET cust_rec.cust_num = 345
    LET cust_rec.cust_name = "McMaclum"
    LET cust_rec.order_ids[1] = 4732
    LET cust_rec.order_ids[2] = 9834
    LET cust_rec.order_ids[3] = 2194
    LET js = util.JSON.stringify( cust_rec )
    DISPLAY util.JSON.format( js )
END MAIN}



