##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE js STRING
    LET js='{ "cust_num":2735, "cust_name":"McCarlson", "order_ids":[234,3456,24656,34561] }'
    DISPLAY util.JSON.proposeType(js)

  CALL fgl_getkey()
END MAIN