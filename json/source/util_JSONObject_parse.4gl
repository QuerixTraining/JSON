##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
    DEFINE js STRING
    DEFINE obj util.JSONObject
    LET js='{ "cust_num":2735, "cust_name":"McCarlson",
              "orderids":[234,3456,24656,34561],
              "js_obj" : { "js_cust_num":1111, "js_cust_name":"js_McCarlson",
              "js_orderids":[123,2345,34567,45678] } }'
    LET obj = util.JSONObject.parse(js)

    IF obj IS NULL THEN DISPLAY "Failed to CREATE the object" ELSE 
      DISPLAY "PASSED"
    END IF
    
  CALL fgl_getkey()
END MAIN

--MAIN
--  DEFINE js STRING
--  DEFINE errorMessage STRING
--  DEFINE obj util.JSONObject
--    LET js='{ "cust_num":2735, "cust_name":"McCarlson",
--              "orderids":[234,3456,24656,34561],
--              "js_obj" : { "js_cust_num":1111, "js_cust_name":"js_McCarlson",
--              "js_orderids":[123,2345,34567,45678] } }'
--    LET obj = util.JSONObject.parse( js )

--    LET errorMessage = ""
--    IF obj.get("cust_num") != 2735 THEN
--        LET errorMessage = errorMessage.append("Step 1. Incorrect cust_num")
--    END IF

--    IF obj.get("cust_name") != "McCarlson" THEN
--        LET errorMessage = errorMessage.append("Step 2. Incorrect cust_name")
--    END IF

--    IF obj.get("js_obj") != '{ "js_cust_num":1111, "js_cust_name":"js_McCarlson",
--              "js_orderids":[123,2345,34567,45678] } }' THEN
--        LET errorMessage = errorMessage.append("Step 3. Incorrect js_obj")
--   END IF

--    IF errorMessage IS NULL THEN
--        DISPLAY "PASSED"
--    ELSE 
--        DISPLAY errorMessage
--    END IF
    
--  CALL fgl_getkey()
--END MAIN