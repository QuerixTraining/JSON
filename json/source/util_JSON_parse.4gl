##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DEFINE error_message STRING
MAIN
    DEFINE cust_rec RECORD
               cust_num INTEGER,
               cust_name VARCHAR(30),
               order_ids DYNAMIC ARRAY OF INTEGER
           END RECORD
    DEFINE js STRING
    
    LET js='{ "cust_num":2735, "cust_name":"McCarlson", "order_ids":[234,3456,24656,34561] }'
    CALL util.JSON.parse(js, cust_rec)
    DISPLAY cust_rec

CALL fgl_getkey()
END MAIN
    
{END MAIN

DEFINE error_message STRING
MAIN
    DEFINE cust_rec RECORD
               cust_num INTEGER,
               cust_name VARCHAR(30),
               order_ids DYNAMIC ARRAY OF INTEGER
           END RECORD
    DEFINE js STRING
    LET error_message = ""
    --LET js='{ "cust_num":2735, "cust_name":"McCarlson", "order_ids":[234,3456,24656,34561] }'
    {CALL util.JSON.parse( js, cust_rec )

    IF cust_rec.cust_name != "McCarlson" THEN 
      LET error_message = error_message.append("Step 1. Error parsing cust_name. ")
    END IF
    
    IF cust_rec.order_ids[4] != 34561 THEN 
      LET error_message = error_message.append("Step 2. Error parsing order_ids[4]. ")
    END IF
    
    IF cust_rec.cust_num != 2735 THEN 
      LET error_message = error_message.append("Step 3. Error parsing cust_num. ")
    END IF
    
    IF error_message IS NULL THEN 
      DISPLAY "PASSED"
    ELSE 
      DISPLAY error_message
    END IF
    
END MAIN}