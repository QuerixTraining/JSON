##########################################################################
# JSON Project                                     #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
  DEFINE json_arr util.JSONArray
    LET json_arr = util.JSONArray.parse('[123,"abc",null]')
    DISPLAY json_arr.getType(1)
    DISPLAY json_arr.getType(2)
    DISPLAY json_arr.getType(3)
  CALL fgl_getkey()	
END MAIN


{MAIN
  DEFINE json_arr util.JSONArray
  DEFINE i INTEGER
    LET json_arr = util.JSONArray.parse('[123,"abc",null]')
    
    FOR i = 1 TO json_arr.getLength()
      DISPLAY i, ": ", json_arr.getType(i)
    END FOR
  CALL fgl_getkey()	
END MAIN}
