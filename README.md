This demo project is intended to show and display JSON integration with Lycia.

You can learn about LJSON functionality from Lycia online documentation:
JSON - http://querix.com/go/docs_online/#auxiliary_features/json/json_intro.htm
JSON data types - http://querix.com/go/docs_online/#auxiliary_features/json/json_classes/json_datatypes.htm
util.JSON class - http://querix.com/go/docs_online/#auxiliary_features/json/json_classes/util_json_class/util.json_class.htm
util.JSONObject class - http://querix.com/go/docs_online/#auxiliary_features/json/json_classes/util_jsonobject_class/util.jsonobject_class.htm
util.JSONArray class - http://querix.com/go/docs_online/#auxiliary_features/json/json_classes/util_jsonarray_class/util.jsonarray_class.htm


To be able to try and use this project, you have to perform the following steps.
1. Register at Querix web-site: http://querix.com/register/
2. Download the latest Lycia III version: http://querix.com/lycia/downloads/
3. Install Lycia to your computer: http://querix.com/go/docs_online/#installation/win_install.htm 
4. Activate the trial license (1 license per 1 developer): http://querix.com/go/docs_online/#licensing/trial.htm (Please contact us if you need more compilation seats. We'd also appreciate it if you let us know the number of developers who will be engaged in this project.)
5. Import this project from the GIT repository via LyciaStudio: http://querix.com/go/docs_online/#developers_guide/working_with_repositories/git/git_perspective_and_views/git_repos_view/clone_repo.htm

Use the link below to download the source files. Here is the link to be cloned: https://gitlab.com/QuerixTraining/JSON.git

You can read about working with GIT repositories in Lycia online documentation: http://querix.com/go/docs_online/#developers_guide/working_with_repositories/repos.htm

After importing this demo project to your workspace, you have to build and run it from LyciaStudio:
http://querix.com/go/docs_online/lyciastudio/build/building_projects/building_and_compilation.htm
http://querix.com/go/docs_online/lyciastudio/run/running_applications/running_applications.htm

Please clear your browsing data before launching the application.

######################################

This project contains demo applications for Querix products.
This software is free but can be used and modified only with Lycia.
Feel free to give your suggestions about enriching this site. 
You can send your ideas and 4gl code to support@querix.com

